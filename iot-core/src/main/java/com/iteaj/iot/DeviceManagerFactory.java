package com.iteaj.iot;

import com.iteaj.iot.server.ServerComponent;

public interface DeviceManagerFactory {

    /**
     * 创建协议管理器
     * @param component
     * @return
     */
    DeviceManager createDeviceManager(ServerComponent component);

}
