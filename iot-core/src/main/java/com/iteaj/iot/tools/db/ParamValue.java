package com.iteaj.iot.tools.db;

public class ParamValue {

    private Object fieldValue;

    private FieldMeta fieldMeta;

    public ParamValue(FieldMeta fieldMeta, Object fieldValue) {
        this.fieldValue = fieldValue;
        this.fieldMeta = fieldMeta;
    }

    public Object getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(Object fieldValue) {
        this.fieldValue = fieldValue;
    }

    public FieldMeta getFieldMeta() {
        return fieldMeta;
    }

    public void setFieldMeta(FieldMeta fieldMeta) {
        this.fieldMeta = fieldMeta;
    }
}
