package com.iteaj.iot.serial;

import com.iteaj.iot.ProtocolException;

public class SerialProtocolException extends ProtocolException {

    public SerialProtocolException(Object protocol) {
        super(protocol);
    }

    public SerialProtocolException(String message) {
        super(message);
    }

    public SerialProtocolException(String message, Throwable cause) {
        super(message, cause);
    }

    public SerialProtocolException(Throwable cause) {
        super(cause);
    }
}
