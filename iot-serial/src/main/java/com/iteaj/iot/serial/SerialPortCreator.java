package com.iteaj.iot.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.iteaj.iot.ProtocolException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 串口创建器
 */
public class SerialPortCreator {

    /**
     * 获取已经打开的串口
     * @param com
     * @return
     */
    public static Optional<SerialClient> get(String com) {
        return Optional.ofNullable(getIotClient(new SerialConnectProperties(com)));
    }

    /**
     * 返回可用的串口列表
     * @return
     */
    public static List<SerialPort> available() {
        return Arrays.asList(SerialPort.getCommPorts());
    }

    /**
     * 串口是否打开
     * @param com
     * @return
     */
    public static boolean isOpen(String com) {
        Optional<SerialClient> client = get(com);
        return client.isPresent() ? client.get().isOpen() : false;
    }

    /**
     * 打开已经存在的串口
     * @param com
     * @return
     */
    public static SerialClient opened(String com) {
        SerialClient serialClient = getIotClient(new SerialConnectProperties(com));
        if(serialClient == null) {
            throw new SerialProtocolException("串口不存在["+com+"]");
        } else if(!serialClient.isOpen()) {
            serialClient.connect(null, 100);
        }

        return serialClient;
    }

    /**
     * 关闭指定串口
     * @param com
     * @return
     */
    public static boolean close(String com) {
        SerialClient client = SerialComponent.instance().getClient(com);
        if(client != null) {
            return client.close();
        } else {
            return false;
        }
    }

    /**
     * 打开指定串口
     * @param properties
     * @return
     */
    public static synchronized SerialClient open(SerialConnectProperties properties) {
        SerialClient serialClient = getIotClient(properties);
        if(serialClient == null) {
            return SerialComponent.instance().createNewClientAndConnect(properties);
        }

        if(!serialClient.isOpen()) {
            if(!serialClient.connect(null, 100)) {
                throw new SerialProtocolException("打开串口失败["+properties+"]");
            }
        }

        return serialClient;
    }

    /**
     * 打开指定串口
     * @param properties
     * @return
     */
    protected static synchronized SerialClient open(SerialConnectProperties properties, SerialPortDataListener listener) {
        SerialClient serialClient = getIotClient(properties);
        if(serialClient == null) {
            return SerialComponent.instance().createNewClientAndConnect(properties, listener);
        }

        if(!serialClient.isOpen()) {
            if(serialClient.getListener() != null) {
                serialClient.removeDataListener();
                serialClient.addDataListener(listener);
            }

            if(!serialClient.connect(null, 100)) {
                throw new SerialProtocolException("打开串口失败["+properties+"]");
            }
        }

        return serialClient;
    }

    /**
     * 异步打开串口
     * @see SerialPort#TIMEOUT_READ_BLOCKING 此方法将导致阻塞方式无效
     * @param connectProperties 串口配置
     * @param packetSize 读取多少到此长度后返回  如果为0则只要有数据就读取
     * @return
     */
    public static SerialClient openByAsync(SerialConnectProperties connectProperties, int packetSize, SerialEventProtocolHandle handle) {
        return open(connectProperties, new SerialPortPacketProtocolListener(packetSize, connectProperties, handle));
    }

    /**
     * 异步打开串(包含分隔符)
     * @see SerialPort#TIMEOUT_READ_BLOCKING 此方法将导致阻塞方式无效
     * @param connectProperties 串口配置
     * @param delimiter 完整报文之间的分隔符
     * @return
     */
    public static SerialClient openByAsync(SerialConnectProperties connectProperties, byte[] delimiter, SerialEventProtocolHandle handle) {
        return openByAsync(connectProperties, delimiter, handle, true);
    }

    /**
     * 异步打开串(包含分隔符)
     * @see SerialPort#TIMEOUT_READ_BLOCKING 此方法将导致阻塞方式无效
     * @param connectProperties 串口配置
     * @param delimiter 完整报文之间的分隔符
     * @param endOfMessage 分隔符是否在报文的尾部
     * @return
     */
    public static SerialClient openByAsync(SerialConnectProperties connectProperties, byte[] delimiter, SerialEventProtocolHandle handle, boolean endOfMessage) {
        return open(connectProperties, new SerialPortDelimiterListener(delimiter, connectProperties, handle, endOfMessage));
    }

    protected static SerialClient getIotClient(SerialConnectProperties properties) throws ProtocolException {
        return SerialComponent.instance().getClient(properties);
    }
}
