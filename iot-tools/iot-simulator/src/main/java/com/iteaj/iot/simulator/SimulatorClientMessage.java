package com.iteaj.iot.simulator;

import com.iteaj.iot.client.ClientMessage;
import com.iteaj.iot.message.DefaultMessageHead;

public class SimulatorClientMessage extends ClientMessage {

    private SimulatorConnectProperties properties;

    public SimulatorClientMessage(byte[] message) {
        super(message);
    }

    public SimulatorClientMessage(MessageHead head) {
        super(head);
    }

    public SimulatorClientMessage(MessageHead head, MessageBody body) {
        super(head, body);
    }

    @Override
    protected MessageHead doBuild(byte[] message) {
        return new DefaultMessageHead(properties.connectKey(), getChannelId(), properties.getType());
    }

    public SimulatorConnectProperties getProperties() {
        return properties;
    }

    public SimulatorClientMessage setProperties(SimulatorConnectProperties properties) {
        this.properties = properties; return this;
    }
}
