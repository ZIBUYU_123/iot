package com.iteaj.iot.taos;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.handle.proxy.ProtocolHandleProxy;

public interface TaosHandle<T extends Protocol> extends ProtocolHandleProxy<T>, IotTaos {

    /**
     * @param protocol
     * @return
     */
    @Override
    Object handle(T protocol);

}
