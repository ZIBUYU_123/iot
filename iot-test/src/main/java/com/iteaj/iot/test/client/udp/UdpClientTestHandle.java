package com.iteaj.iot.test.client.udp;

import com.iteaj.iot.boot.autoconfigure.IotServerProperties;
import com.iteaj.iot.client.ClientProtocolHandle;
import com.iteaj.iot.client.udp.UdpClientConnectProperties;
import com.iteaj.iot.server.udp.impl.DefaultUdpServerComponent;
import com.iteaj.iot.test.IotTestHandle;
import com.iteaj.iot.test.IotTestProperties;
import com.iteaj.iot.test.TestConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@ConditionalOnProperty(prefix = "iot.test", name = "udp.start", havingValue = "true")
public class UdpClientTestHandle implements IotTestHandle, ClientProtocolHandle<UdpClientServerInitProtocol> {

    private final IotServerProperties properties;
    private final IotTestProperties testProperties;
    private final DefaultUdpServerComponent component;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public UdpClientTestHandle(IotServerProperties properties
            , IotTestProperties testProperties
            , DefaultUdpServerComponent defaultUdpServerComponent) {
        this.properties = properties;
        this.testProperties = testProperties;
        this.component = defaultUdpServerComponent;
    }

    @Override
    public void start() throws Exception {
        System.out.println("------------------------------------------------ 开始UDP协议测试 -----------------------------------------------------");

        String host = testProperties.getHost();
        IotTestProperties.TestUdpConnectConfig udp = testProperties.getUdp();
        host = udp.getHost() == null ? host : udp.getHost();
        UdpClientConnectProperties connectProperties = new UdpClientConnectProperties(host, udp.getPort());
        new UdpClientTestInitProtocol("test Udp msg".getBytes()).request(connectProperties, protocol -> {

        });

        IotServerProperties.DefaultUdpConnectProperties udp1 = properties.getUdp();
        UdpClientConnectProperties properties = new UdpClientConnectProperties(host, udp1.getPort());
        new UdpClientTestInitProtocol("UDP".getBytes()).request(properties);

        TimeUnit.SECONDS.sleep(2);
    }

    @Override
    public int getOrder() {
        return 1000 * 2;
    }

    @Override
    public Object handle(UdpClientServerInitProtocol protocol) {
        UdpClientTestMessage message = protocol.requestMessage();
        String equipCode = message.getHead().getEquipCode();
        String msg = new String(message.getMessage());
        if(msg.equals("UDP:req")) {
            logger.info(TestConst.LOGGER_PROTOCOL_FUNC_DESC
                    , component.getName(), "write", equipCode, "通过");
        } else if(msg.equals("UDP:req(deviceSn)")) {
            logger.info(TestConst.LOGGER_PROTOCOL_FUNC_DESC
                    , component.getName(), "write+deviceSn", equipCode, "通过");
        } else if(msg.equals("UDP:resp")) {
            logger.info(TestConst.LOGGER_PROTOCOL_FUNC_DESC
                    , component.getName(), "response", equipCode, "通过");
        } else {
//            logger.error(TestConst.LOGGER_PROTOCOL_FUNC_DESC
//                    , component.getName(), msg, equipCode, "失败");
        }

        return null;
    }
}
