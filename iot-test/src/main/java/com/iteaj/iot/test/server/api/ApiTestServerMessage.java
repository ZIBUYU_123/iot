package com.iteaj.iot.test.server.api;

import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.server.ServerMessage;

public class ApiTestServerMessage extends ServerMessage {

    public ApiTestServerMessage(byte[] message) {
        super(message);
    }

    public ApiTestServerMessage(MessageHead head) {
        super(head);
    }

    public ApiTestServerMessage(MessageHead head, MessageBody body) {
        super(head, body);
    }

    @Override
    protected MessageHead doBuild(byte[] message) {
        String msg = new String(message);
        String[] split = msg.split(":");
        String deviceSn = split[2];
        ApiProtocolType instance = ApiProtocolType.getInstance(split[1]);
        return new DefaultMessageHead(deviceSn, instance, message);
    }
}
